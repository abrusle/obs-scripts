# OBS Scripts

This repo is a collection of utility scripts to extend some of OBS's feature set.

## Scritps Presentation

### Current Date & Time

`lua/current_date-time.lua` is a lua script that sets a Text source's content to the current date and time, along a given format.

![](docs/images/current_date-time.png)

### Spotify Song Name

`powershell/spotify_song_name.ps1` is a powershell script that writes the current song's name and artist to a file that you can then tell OBS to display.

**Requirements**
- Windows only
- Use Spotify desktop client
- Set the script execution policy on your compute to allow running this script (see [About Execution Policies | Microsoft Learn](https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-7.4))

⚠️ Honestly this script is a **dirty hack**, and I don't know how long it is going to work. The script tries to find the spotify process and extracts its window title. At the time of writing this, the window title contains the song and artist names.

**Possible improvements**
- Convert to a python/lua script and use the official Sprotify API
- Access the general windows media API from powershell (you know, the thing that allows different apps to show a currently running video/song).