-- Show the current date with an optional formatting
-- Author: Hector Leiva (hex@hectorleiva)
-- Version 1.0

--[[
Usage:
    Format: Current Date: YYYY-MM-DD is the current date
--]]

obs = obslua

SOURCE_NAME = ""
FORMAT = "%c"
REFRESH_INTERVAL = 60

-- Loads all the settings possible for the script
function script_properties()
    local props = obs.obs_properties_create()
    
    -- (obs_properties, setting id, description(localized name shown to user), type, format)
    local p = obs.obs_properties_add_list(props, "source", "Text Source", obs.OBS_COMBO_TYPE_EDITABLE, obs.OBS_COMBO_FORMAT_STRING)
    local sources = obs.obs_enum_sources()

    -- As long as the sources are not empty, then
    if sources ~= nil then
        -- iterate over all the sources
        for _, source in ipairs(sources) do
            source_id = obs.obs_source_get_id(source)
            if source_id == "text_gdiplus" or source_id == "text_ft2_source" or source_id == "text_gdiplus_v2" or source_id == "text_ft2_source_v2" then
                local name = obs.obs_source_get_name(source)
                obs.obs_property_list_add_string(p, name, name)
            end
        end
    end

    obs.source_list_release(sources)

    obs.obs_properties_add_text(props, "format", "Date Format", obs.OBS_TEXT_DEFAULT)
    obs.obs_properties_add_int(props, "refresh_interval", "Refresh Interval (Seconds)", 1, 3600, 1)
    
    return props
end

function script_description()
    return [[
        <h1>Current Date and Time</h1>
        <p>Sets the current date in the selected Text source.<p>

        <h2>Date Formatting Special Characters:</h2>

        <style>
        #format_table td {
            padding: 0.2em;
        }
        </style>
        <table id="format_table" align="center" style="border-collapse: collapse;" border=1>
<tbody>
<tr><td><code>%a</code></td><td>abbreviated weekday name (e.g., <code>Wed</code>)</td></tr>
<tr><td><code>%A</code></td><td>full weekday name (e.g., <code>Wednesday</code>)</td></tr>
<tr><td><code>%b</code></td><td>abbreviated month name (e.g., <code>Sep</code>)</td></tr>
<tr><td><code>%B</code></td><td>full month name (e.g., <code>September</code>)</td></tr>
<tr><td><code>%c</code></td><td>date and time (e.g., <code>09/16/98 23:48:10</code>)</td></tr>
<tr><td><code>%d</code></td><td>day of the month (<code>16</code>) [01-31]</td></tr>
<tr><td><code>%H</code></td><td>hour, using a 24-hour clock (<code>23</code>) [00-23]</td></tr>
<tr><td><code>%I</code></td><td>hour, using a 12-hour clock (<code>11</code>) [01-12]</td></tr>
<tr><td><code>%M</code></td><td>minute (<code>48</code>) [00-59]</td></tr>
<tr><td><code>%m</code></td><td>month (<code>09</code>) [01-12]</td></tr>
<tr><td><code>%p</code></td><td>either <code>"am"</code> or <code>"pm"</code> (<code>pm</code>)</td></tr>
<tr><td><code>%S</code></td><td>second (<code>10</code>) [00-61]</td></tr>
<tr><td><code>%w</code></td><td>weekday (<code>3</code>) [0-6 = Sunday-Saturday]</td></tr>
<tr><td><code>%x</code></td><td>date (e.g., <code>09/16/98</code>)</td></tr>
<tr><td><code>%X</code></td><td>time (e.g., <code>23:48:10</code>)</td></tr>
<tr><td><code>%Y</code></td><td>full year (<code>1998</code>)</td></tr>
<tr><td><code>%y</code></td><td>two-digit year (<code>98</code>)  [00-99]</td></tr>
<tr><td><code>%%</code></td><td>the character '<code>%</code>'</td></tr>
</tbody></table>
        <p>More info on date formatting at <a href="https://www.lua.org/pil/22.1.html">https://www.lua.org/pil/22.1.html</a></p>
    ]]
end

-- script_defaults will be called to set the initial default settings
function script_defaults(settings)
    obs.obs_data_set_default_string(settings, "format", FORMAT)
    obs.obs_data_set_default_int(settings, "refresh_interval", REFRESH_INTERVAL)

end

-- script_update will be called when settings are changed
function script_update(settings)
    SOURCE_NAME = obs.obs_data_get_string(settings, "source")
    FORMAT = obs.obs_data_get_string(settings, "format")
    REFRESH_INTERVAL = obs.obs_data_get_int(settings, "refresh_interval")
    restart_refresh_timer()
end

-- script_load will be called whenever a new source is created
function script_load(settings)
    local sh = obs.obs_get_signal_handler()
    obs.signal_handler_connect(sh, "source_activate", source_activated)
end

function source_activated(cd)
    activate_plugin(cd)
end

-- Called when this source is activated
function activate_plugin(cd)
    local source = obs.calldata_source(cd, "source")
    if source ~= nil then
        local name = obs.obs_source_get_name(source)
        if (name == SOURCE_NAME) then
            activate()
        end
    end
end

function activate()
    set_current_date_text()
    restart_refresh_timer()
end

function restart_refresh_timer()
    obs.timer_remove(set_current_date_text)
    if (REFRESH_INTERVAL > 0) then
        obs.timer_add(set_current_date_text, REFRESH_INTERVAL * 1000)
    end
end

function set_current_date_text()
    local date_format = FORMAT
    if (date_format == '' or date_format == nil) then
	    date_format = '%c'
    end

    local date_string_text = os.date(date_format)
    print("Setting date_string_text: "..date_string_text)

    local source = obs.obs_get_source_by_name(SOURCE_NAME)

    if source ~= nil then
        local settings = obs.obs_data_create()
        obs.obs_data_set_string(settings, "text", date_string_text)
        obs.obs_source_update(source, settings)
        obs.obs_data_release(settings)
        obs.obs_source_release(source)
    end
end
