[System.Console]::Title = "Spotify Song Name Updater"
$outputFilePath = $pwd.Path + "\spotify-song.html"
$maxLength = 55

while ($true)
{
    $spotifyProcess = Get-Process -Name "Spotify"
    $fileContents = " "

    if ($spotifyProcess -ne $null)
    {
        $fileContents = $spotifyProcess.MainWindowTitle.Trim() -join "" # -join to remove new line characters
	if ($fileContents.length -ge $maxLength)
	{
		$fileContents = $fileContents.substring(0, $maxLength) + "…"
	}

	    if ($fileContents -eq "Spotify Premium") # Happens when you pause playback.
	    {
	        $fileContents = "- Pause -"
	    }
    }

    Write-Host $fileContents
    $fileContents | Out-File -FilePath $outputFilePath -Encoding UTF8 -Force

    Start-Sleep -Seconds 2
}
